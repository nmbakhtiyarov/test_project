<?php

/**
 * test_project - QuestionDTO.php
 *
 * Created by: nmbakhtiyarov@gmail.com
 * Initial version created on: 01.08.2019
 */

namespace FileHandler\DTO;


class QuestionDTO
{
    /**
     * Тема из начала группы вопросов. К примеру, Тестирование №1 "Клетка и макронутриенты"
     * @var string $theme
     */
    public $theme;
    /**
     * Номер вопроса
     * @var int $number
     */
    public $number;
    /**
     * Категория вопроса из последней колонки
     * @var string $category
     */
    public $category;
    /**
     * Текст вопроса
     * @var string $questionText
     */
    public $questionText;
    /**
     * Массив ответов
     * @var array $answers
     */
    public $answers=[];
    /**
     * Массив правильных ответов, можно использовать ключи предыдущего массива
     * @var array $rightAnswers
     */
    public $rightAnswers=[];
    /**
     * Множитель правильных ответов
     * @var float $correctAnswerMultiplier
     */
    public $correctAnswerMultiplier;
    /**
     * Множитель неправильных ответов. Если отсутствует в таблице, значит они совпадают
     * @var float $correctAnswerMultiplier
     */
    public $errorAnswerMultiplier;
}
