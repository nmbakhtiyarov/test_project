<?php

/**
 * test_project - XlsxHandler.php
 *
 * Created by: nmbakhtiyarov@gmail.com
 * Initial version created on: 01.08.2019
 */

namespace FileHandler;


use FileHandler\DTO\QuestionDTO;

class XlsxHandler extends AbstractHandler
{
    private const MESSAGE = 'Hello, World!';

    public function parse()
    {
        echo $this->getDefaultString();
    }

    protected function getDefaultString()
    {
        return $this::MESSAGE;
    }
}
